﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitter_Test_Program
{
    public class Config
    {
        public string API_KEY;
        public string API_SECRET_KEY;
        public string ACCESS_TOKEN;
        public string ACCESS_TOKEN_SECRET;

        public Config()
        {

        }
    }
}
