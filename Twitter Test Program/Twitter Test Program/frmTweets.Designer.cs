﻿namespace Twitter_Test_Program
{
    partial class frmTweets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panSideBar = new System.Windows.Forms.Panel();
            this.lblCurrentTime = new System.Windows.Forms.Label();
            this.btnSchedule = new System.Windows.Forms.Button();
            this.lblSizeCount = new System.Windows.Forms.Label();
            this.txtTweet = new System.Windows.Forms.TextBox();
            this.panTime = new System.Windows.Forms.Panel();
            this.timePicker = new System.Windows.Forms.DateTimePicker();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.lblStatus = new System.Windows.Forms.Label();
            this.groupLocation = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radASAP = new System.Windows.Forms.RadioButton();
            this.radSchedule = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listTweetsToBeSent = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panSideBar.SuspendLayout();
            this.panTime.SuspendLayout();
            this.groupLocation.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panSideBar
            // 
            this.panSideBar.Controls.Add(this.lblCurrentTime);
            this.panSideBar.Controls.Add(this.btnSchedule);
            this.panSideBar.Controls.Add(this.lblSizeCount);
            this.panSideBar.Controls.Add(this.txtTweet);
            this.panSideBar.Controls.Add(this.panTime);
            this.panSideBar.Controls.Add(this.lblStatus);
            this.panSideBar.Controls.Add(this.groupLocation);
            this.panSideBar.Controls.Add(this.label2);
            this.panSideBar.Controls.Add(this.label1);
            this.panSideBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.panSideBar.Location = new System.Drawing.Point(0, 0);
            this.panSideBar.Name = "panSideBar";
            this.panSideBar.Size = new System.Drawing.Size(233, 560);
            this.panSideBar.TabIndex = 1;
            // 
            // lblCurrentTime
            // 
            this.lblCurrentTime.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCurrentTime.Location = new System.Drawing.Point(0, 425);
            this.lblCurrentTime.Name = "lblCurrentTime";
            this.lblCurrentTime.Size = new System.Drawing.Size(233, 13);
            this.lblCurrentTime.TabIndex = 14;
            this.lblCurrentTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSchedule
            // 
            this.btnSchedule.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSchedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSchedule.Location = new System.Drawing.Point(0, 393);
            this.btnSchedule.Name = "btnSchedule";
            this.btnSchedule.Size = new System.Drawing.Size(233, 32);
            this.btnSchedule.TabIndex = 13;
            this.btnSchedule.Text = "Schedule";
            this.btnSchedule.UseVisualStyleBackColor = true;
            this.btnSchedule.Click += new System.EventHandler(this.btnSchedule_Click);
            // 
            // lblSizeCount
            // 
            this.lblSizeCount.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSizeCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSizeCount.Location = new System.Drawing.Point(0, 370);
            this.lblSizeCount.Name = "lblSizeCount";
            this.lblSizeCount.Size = new System.Drawing.Size(233, 23);
            this.lblSizeCount.TabIndex = 12;
            this.lblSizeCount.Text = "280";
            this.lblSizeCount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtTweet
            // 
            this.txtTweet.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTweet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTweet.Location = new System.Drawing.Point(0, 212);
            this.txtTweet.MaxLength = 280;
            this.txtTweet.Multiline = true;
            this.txtTweet.Name = "txtTweet";
            this.txtTweet.Size = new System.Drawing.Size(233, 158);
            this.txtTweet.TabIndex = 10;
            this.txtTweet.TextChanged += new System.EventHandler(this.txtTweet_TextChanged);
            // 
            // panTime
            // 
            this.panTime.Controls.Add(this.timePicker);
            this.panTime.Controls.Add(this.datePicker);
            this.panTime.Dock = System.Windows.Forms.DockStyle.Top;
            this.panTime.Location = new System.Drawing.Point(0, 190);
            this.panTime.Name = "panTime";
            this.panTime.Size = new System.Drawing.Size(233, 22);
            this.panTime.TabIndex = 8;
            // 
            // timePicker
            // 
            this.timePicker.CustomFormat = "hh:mm:ss";
            this.timePicker.Dock = System.Windows.Forms.DockStyle.Left;
            this.timePicker.Enabled = false;
            this.timePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timePicker.Location = new System.Drawing.Point(114, 0);
            this.timePicker.Name = "timePicker";
            this.timePicker.ShowUpDown = true;
            this.timePicker.Size = new System.Drawing.Size(119, 20);
            this.timePicker.TabIndex = 7;
            // 
            // datePicker
            // 
            this.datePicker.CustomFormat = "MM/dd/yyyy";
            this.datePicker.Dock = System.Windows.Forms.DockStyle.Left;
            this.datePicker.Enabled = false;
            this.datePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datePicker.Location = new System.Drawing.Point(0, 0);
            this.datePicker.Name = "datePicker";
            this.datePicker.Size = new System.Drawing.Size(114, 20);
            this.datePicker.TabIndex = 6;
            // 
            // lblStatus
            // 
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(0, 517);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(233, 43);
            this.lblStatus.TabIndex = 6;
            // 
            // groupLocation
            // 
            this.groupLocation.Controls.Add(this.tableLayoutPanel1);
            this.groupLocation.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupLocation.Location = new System.Drawing.Point(0, 101);
            this.groupLocation.Name = "groupLocation";
            this.groupLocation.Size = new System.Drawing.Size(233, 89);
            this.groupLocation.TabIndex = 2;
            this.groupLocation.TabStop = false;
            this.groupLocation.Text = "Tweet Times";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.radASAP, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radSchedule, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(227, 70);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // radASAP
            // 
            this.radASAP.AutoSize = true;
            this.radASAP.Checked = true;
            this.radASAP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radASAP.Location = new System.Drawing.Point(3, 3);
            this.radASAP.Name = "radASAP";
            this.radASAP.Size = new System.Drawing.Size(69, 64);
            this.radASAP.TabIndex = 0;
            this.radASAP.TabStop = true;
            this.radASAP.Text = "ASAP";
            this.radASAP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radASAP.UseVisualStyleBackColor = true;
            this.radASAP.CheckedChanged += new System.EventHandler(this.radASAP_CheckedChanged);
            // 
            // radSchedule
            // 
            this.radSchedule.AutoSize = true;
            this.radSchedule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSchedule.Location = new System.Drawing.Point(153, 3);
            this.radSchedule.Name = "radSchedule";
            this.radSchedule.Size = new System.Drawing.Size(71, 64);
            this.radSchedule.TabIndex = 1;
            this.radSchedule.Text = "Schedule";
            this.radSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radSchedule.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(78, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 70);
            this.label3.TabIndex = 2;
            this.label3.Text = "or ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Location = new System.Drawing.Point(0, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(233, 33);
            this.label2.TabIndex = 1;
            this.label2.Text = "When do you want to send your Tweet?";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(233, 68);
            this.label1.TabIndex = 0;
            this.label1.Text = "Twitter Tweet Form";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listTweetsToBeSent
            // 
            this.listTweetsToBeSent.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listTweetsToBeSent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listTweetsToBeSent.FullRowSelect = true;
            this.listTweetsToBeSent.GridLines = true;
            this.listTweetsToBeSent.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listTweetsToBeSent.HideSelection = false;
            this.listTweetsToBeSent.Location = new System.Drawing.Point(233, 0);
            this.listTweetsToBeSent.MultiSelect = false;
            this.listTweetsToBeSent.Name = "listTweetsToBeSent";
            this.listTweetsToBeSent.Size = new System.Drawing.Size(767, 560);
            this.listTweetsToBeSent.TabIndex = 2;
            this.listTweetsToBeSent.UseCompatibleStateImageBehavior = false;
            this.listTweetsToBeSent.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Tweet";
            this.columnHeader1.Width = 500;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Date and Time";
            this.columnHeader2.Width = 150;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmTweets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 560);
            this.Controls.Add(this.listTweetsToBeSent);
            this.Controls.Add(this.panSideBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmTweets";
            this.Text = "frmTweets";
            this.panSideBar.ResumeLayout(false);
            this.panSideBar.PerformLayout();
            this.panTime.ResumeLayout(false);
            this.groupLocation.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panSideBar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView listTweetsToBeSent;
        private System.Windows.Forms.GroupBox groupLocation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radSchedule;
        private System.Windows.Forms.RadioButton radASAP;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Panel panTime;
        private System.Windows.Forms.DateTimePicker timePicker;
        private System.Windows.Forms.DateTimePicker datePicker;
        private System.Windows.Forms.TextBox txtTweet;
        private System.Windows.Forms.Button btnSchedule;
        private System.Windows.Forms.Label lblSizeCount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblCurrentTime;
    }
}