﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tweetinvi;
using Tweetinvi.Models;

namespace Twitter_Test_Program
{
    public partial class frmHome : Form
    {
        ITwitterCredentials creds;
        IAuthenticatedUser authenticatedUser;
        List<Form> listForms;
        bool fullSizeSideBar;
        public frmHome(Config config)
        {
            InitializeComponent();
            listForms = new List<Form>();
            creds = new TwitterCredentials(config.API_KEY, config.API_SECRET_KEY, config.ACCESS_TOKEN, config.ACCESS_TOKEN_SECRET);
            Auth.SetCredentials(creds);
            authenticatedUser = User.GetAuthenticatedUser(creds);
            createForms();
            setMainPanelForm(0);
            fullSizeSideBar = true;
        }

        /* Form Movement Controls */
        private bool mouseDown;
        private Point lastLocation;
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        /* Button Clicks */
        //Top Bar
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        
        //Side Bar
        private void btnDashboard_Click(object sender, EventArgs e)
        {
            setMainPanelForm(0);
            btnDashboard.BackColor = Color.FromArgb(170, 184, 194);
        }
        private void btnAnalytics_Click(object sender, EventArgs e)
        {
            setMainPanelForm(1);
        }
        private void BtnSchedule_Click(object sender, EventArgs e)
        {
            setMainPanelForm(2);
        }
        private void btnFriends_Click(object sender, EventArgs e)
        {
            setMainPanelForm(3);
        }
        private void btnMinimizeSideBar_Click(object sender, EventArgs e)
        {
            if (fullSizeSideBar == true)
            {
                panSideBar.Size = new Size(67, 560);
                changeSideBarButtonText(new string[] { "", "", "", "", "" });
                fullSizeSideBar = false;
                btnMinimizeSideBar.ImageIndex = 0;
            }
            else
            {
                panSideBar.Size = new Size(194, 560);
                changeSideBarButtonText(new string[] { "Dashboard", "Analytics", "Schedule", "Friends", "Minimize" });
                fullSizeSideBar = true;
                btnMinimizeSideBar.ImageIndex = 1;

            }
        }
        
        /* Main Form Operations */
        public void setMainPanelForm(int formID)
        {
            String[] formNames = { "frmDashboard", "frmAnalytics", "frmTweets", "frmFriends" };

            Form form = doesFormExist(formNames[formID]);
            if (form == null)
            {
                switch (formID)
                {
                    case 0:
                        form = new frmDashboard(authenticatedUser) { TopLevel = false, TopMost = true };
                        break;
                    case 1:
                        form = new frmAnalytics(authenticatedUser) { TopLevel = false, TopMost = true };
                        break;
                    case 2:
                        form = new frmTweets(authenticatedUser) { TopLevel = false, TopMost = true };
                        break;
                    case 3:
                        form = new frmFriends(authenticatedUser) { TopLevel = false, TopMost = true };
                        break;
                }
                form.FormBorderStyle = FormBorderStyle.None;
                form.Dock = DockStyle.Fill;
                panBody.Controls.Clear(); //Clears Panel Before Placing New Form
                listForms.Add(form);
                this.panBody.Controls.Add(form);
                Console.WriteLine(form.Name);
                form.Show();
            }
            else
            {
                panBody.Controls.Clear();
                this.panBody.Controls.Add(form);
                getCurrentForm(form.Name);
            }

        }
        public Form doesFormExist(String formName)
        {
            Form currentForm = null;
            foreach(Form form in listForms)
            {
                if (form.Name.Equals(formName))
                {
                    currentForm = form;
                    break;
                }
            }
            return currentForm;
        }
        public void hideAllForms()
        {
            foreach(Form form in listForms)
            {
                form.Visible = false;
            }
        }
        public void createForms()
        {
            setMainPanelForm(0);
            setMainPanelForm(1);
            setMainPanelForm(2);
            setMainPanelForm(3);
        }

        public void changeSideBarButtonText(String[] textValues)
        {
            btnDashboard.Text = textValues[0];
            btnAnalytics.Text = textValues[1];
            btnSchedule.Text = textValues[2];
            btnFriends.Text = textValues[3];
            btnMinimizeSideBar.Text = textValues[4];
        }
        public void getCurrentForm(String formName)
        {
            Color selectedColor = Color.FromArgb(170, 184, 194);
            Color notSelectedColor = Color.FromArgb(212, 216, 212);
            Button[] buttons = { btnDashboard, btnAnalytics, btnSchedule, btnFriends };

            int index = getFormIndexFromFormName(formName);

            for (int i = 0; i < 4; i++)
            {
                if (index == i)
                {
                    buttons[i].BackColor = selectedColor;
                }
                else
                {
                    buttons[i].BackColor = notSelectedColor;
                }
            }
        }
        public int getFormIndexFromFormName(String formName)
        {
            int index = -1;
            if (formName.Equals("frmDashboard")) index = 0;
            if (formName.Equals("frmAnalytics")) index = 1;
            if (formName.Equals("frmTweets")) index = 2;
            if (formName.Equals("frmFriends")) index = 3;
            return index;
        }
    }
}
