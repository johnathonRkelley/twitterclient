﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tweetinvi;
using Tweetinvi.Core.Extensions;
using Tweetinvi.Models;

namespace Twitter_Test_Program
{
    public partial class frmTweets : Form
    {
        public int amountOfCharacters;
        private List<SendTweet> tweets;
        IAuthenticatedUser authenticatedUser;

        public frmTweets(IAuthenticatedUser authenticatedUser)
        {
            InitializeComponent();
            this.authenticatedUser = authenticatedUser;
            tweets = new List<SendTweet>();
            amountOfCharacters = 280;
            lblSizeCount.Text = amountOfCharacters.ToString();
        }

        /* Button Clicks */
        private void btnSchedule_Click(object sender, EventArgs e)
        {
            DateTime date = datePicker.Value;
            DateTime time = timePicker.Value;

            DateTime dateTime = new DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, time.Second);

            if (isOldTweet(dateTime) && radASAP.Checked == false)
            {
                lblStatus.Text = "Fix your time... Old Tweet has been detected";
            } else
            {
                lblStatus.Text = "Your Tweet has been added!";
                addTweetData(dateTime);
            }
        }

        /* Form Methods */
        private void radASAP_CheckedChanged(object sender, EventArgs e)
        {
            if (radASAP.Checked == true)
            {
                datePicker.Enabled = false;
                timePicker.Enabled = false;
            } else
            {
                datePicker.Enabled = true;
                timePicker.Enabled = true;
            }
        }
        private void txtTweet_TextChanged(object sender, EventArgs e)
        {
            changeTextAllowed();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            checkForTweetsToBeSent();
            lblCurrentTime.Text = DateTime.Now.ToString();
        }

        /* Helper Methods for Form Methods */
        public void addTweetData(DateTime dateTime)
        {

            if (radASAP.Checked == true)
            {
                sendTweetASAP();
            } else
            {
                SendTweet tweet = new SendTweet(txtTweet.Text, dateTime);

                tweets.Add(tweet);

                listTweetsToBeSent.Items.Add(new ListViewItem(new string[] { txtTweet.Text, dateTime.ToString() }));

            }
            txtTweet.Text = "";

        }
        public void sendTweetASAP()
        {
            _ = Tweet.PublishTweet(txtTweet.Text);
            lblStatus.Text = "Tweet has been sent!";

        }
        public void sendTweet(String tweet)
        {
            _ = Tweet.PublishTweet(tweet);
        }
        public void checkForTweetsToBeSent()
        {
            for (int i = 0; i < tweets.Count; i++) {
                if (isCurrentTimeMatching(tweets[i].dateTime))
                {
                    sendTweet(tweets[i].tweet);
                    lblStatus.Text = "Tweet has been sent!";
                    deleteListViewItem(tweets[i].dateTime);
                    tweets.RemoveAt(i);
                }
            }
        }
        public void changeTextAllowed()
        {
            int currentCharacters = txtTweet.TextLength;
            int remaining = amountOfCharacters - currentCharacters;
            lblSizeCount.Text = remaining.ToString();
        }
        public void deleteListViewItem(DateTime time)
        {
            int index = 0;

            for(int i = 0; i < listTweetsToBeSent.Items.Count; i++)
            {
                if (listTweetsToBeSent.Items[i].SubItems[1].Equals(time.ToLongTimeString())) {
                    index = i;
                }
            }

            listTweetsToBeSent.Items.RemoveAt(index);
        }

        /* Bool Statements */
        public bool isOldTweet(DateTime dateTime)
        {
            DateTime now = DateTime.Now;
            TimeSpan span2 = dateTime - now;

            if (span2.TotalSeconds < 0)
                return true;
            else
                return false;
        }
        public bool isCurrentTimeMatching(DateTime dateTime)
        {
            DateTime now = DateTime.Now;

            if (now.ToShortDateString().Equals(dateTime.ToShortDateString()) && now.ToLongTimeString().Equals(dateTime.ToLongTimeString()))
                return true;
            else
                return false;
        }

    }
}
