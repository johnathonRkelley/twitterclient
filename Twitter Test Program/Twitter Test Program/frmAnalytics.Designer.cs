﻿namespace Twitter_Test_Program
{
    partial class frmAnalytics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panSideBar = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panStats = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblRetweet = new System.Windows.Forms.Label();
            this.lblLikes = new System.Windows.Forms.Label();
            this.lblReplies = new System.Windows.Forms.Label();
            this.link1 = new System.Windows.Forms.LinkLabel();
            this.txtTweet = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCreation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panBody = new System.Windows.Forms.Panel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panSideBar.SuspendLayout();
            this.panStats.SuspendLayout();
            this.panBody.SuspendLayout();
            this.SuspendLayout();
            // 
            // panSideBar
            // 
            this.panSideBar.Controls.Add(this.panStats);
            this.panSideBar.Controls.Add(this.link1);
            this.panSideBar.Controls.Add(this.txtTweet);
            this.panSideBar.Controls.Add(this.label5);
            this.panSideBar.Controls.Add(this.txtCreation);
            this.panSideBar.Controls.Add(this.label2);
            this.panSideBar.Controls.Add(this.label1);
            this.panSideBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.panSideBar.Location = new System.Drawing.Point(0, 0);
            this.panSideBar.Name = "panSideBar";
            this.panSideBar.Size = new System.Drawing.Size(200, 560);
            this.panSideBar.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(69, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 43);
            this.label4.TabIndex = 5;
            this.label4.Text = "Likes";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // panStats
            // 
            this.panStats.ColumnCount = 3;
            this.panStats.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.panStats.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.panStats.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.panStats.Controls.Add(this.label3, 0, 0);
            this.panStats.Controls.Add(this.label4, 1, 0);
            this.panStats.Controls.Add(this.label9, 2, 0);
            this.panStats.Controls.Add(this.lblRetweet, 0, 1);
            this.panStats.Controls.Add(this.lblLikes, 1, 1);
            this.panStats.Controls.Add(this.lblReplies, 2, 1);
            this.panStats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panStats.Location = new System.Drawing.Point(0, 473);
            this.panStats.Name = "panStats";
            this.panStats.RowCount = 2;
            this.panStats.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.panStats.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.panStats.Size = new System.Drawing.Size(200, 87);
            this.panStats.TabIndex = 27;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 43);
            this.label3.TabIndex = 4;
            this.label3.Text = "RTs";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(135, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 43);
            this.label9.TabIndex = 6;
            this.label9.Text = "Replies";
            this.label9.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lblRetweet
            // 
            this.lblRetweet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRetweet.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRetweet.Location = new System.Drawing.Point(3, 43);
            this.lblRetweet.Name = "lblRetweet";
            this.lblRetweet.Size = new System.Drawing.Size(60, 44);
            this.lblRetweet.TabIndex = 7;
            this.lblRetweet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLikes
            // 
            this.lblLikes.AutoSize = true;
            this.lblLikes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLikes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLikes.Location = new System.Drawing.Point(69, 43);
            this.lblLikes.Name = "lblLikes";
            this.lblLikes.Size = new System.Drawing.Size(60, 44);
            this.lblLikes.TabIndex = 8;
            this.lblLikes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblReplies
            // 
            this.lblReplies.AutoSize = true;
            this.lblReplies.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblReplies.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReplies.Location = new System.Drawing.Point(135, 43);
            this.lblReplies.Name = "lblReplies";
            this.lblReplies.Size = new System.Drawing.Size(62, 44);
            this.lblReplies.TabIndex = 9;
            this.lblReplies.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // link1
            // 
            this.link1.Dock = System.Windows.Forms.DockStyle.Top;
            this.link1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.link1.Location = new System.Drawing.Point(0, 410);
            this.link1.Name = "link1";
            this.link1.Size = new System.Drawing.Size(200, 63);
            this.link1.TabIndex = 26;
            this.link1.TabStop = true;
            this.link1.Text = "Link to Tweet";
            this.link1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTweet
            // 
            this.txtTweet.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTweet.Enabled = false;
            this.txtTweet.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTweet.Location = new System.Drawing.Point(0, 244);
            this.txtTweet.Multiline = true;
            this.txtTweet.Name = "txtTweet";
            this.txtTweet.Size = new System.Drawing.Size(200, 166);
            this.txtTweet.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(200, 58);
            this.label5.TabIndex = 24;
            this.label5.Text = "Tweet Content";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCreation
            // 
            this.txtCreation.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtCreation.Enabled = false;
            this.txtCreation.Location = new System.Drawing.Point(0, 166);
            this.txtCreation.Name = "txtCreation";
            this.txtCreation.Size = new System.Drawing.Size(200, 20);
            this.txtCreation.TabIndex = 23;
            this.txtCreation.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 105);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(200, 61);
            this.label2.TabIndex = 22;
            this.label2.Text = "Tweet Creation Time";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 105);
            this.label1.TabIndex = 21;
            this.label1.Text = "Tweet Analytics\r\n(Past 40 Tweets)\r\n";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // panBody
            // 
            this.panBody.Controls.Add(this.listView1);
            this.panBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panBody.Location = new System.Drawing.Point(200, 0);
            this.panBody.Name = "panBody";
            this.panBody.Size = new System.Drawing.Size(800, 560);
            this.panBody.TabIndex = 2;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(800, 560);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Created At";
            this.columnHeader1.Width = 151;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Text Preview";
            this.columnHeader2.Width = 407;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Likes";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 48;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "RTs";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 52;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Replies";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "URL";
            this.columnHeader6.Width = 350;
            // 
            // frmAnalytics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 560);
            this.Controls.Add(this.panBody);
            this.Controls.Add(this.panSideBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAnalytics";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmAnalytics";
            this.panSideBar.ResumeLayout(false);
            this.panSideBar.PerformLayout();
            this.panStats.ResumeLayout(false);
            this.panStats.PerformLayout();
            this.panBody.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panSideBar;
        private System.Windows.Forms.TableLayoutPanel panStats;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblRetweet;
        private System.Windows.Forms.Label lblLikes;
        private System.Windows.Forms.Label lblReplies;
        private System.Windows.Forms.LinkLabel link1;
        private System.Windows.Forms.TextBox txtTweet;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCreation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panBody;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
    }
}