﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tweetinvi.Models;

namespace Twitter_Test_Program
{
    public partial class frmAnalytics : Form
    {
        IAuthenticatedUser authenticatedUser;
        String currentURL;

        public frmAnalytics(IAuthenticatedUser authenticatedUser)
        {
            InitializeComponent();
            this.authenticatedUser = authenticatedUser;

            IEnumerable<ITweet> tweets = authenticatedUser.GetUserTimeline(40);

            buildListView(tweets);
            currentURL = "";
        }

        /* Click Functions */
        private void link1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(currentURL);
        }

        /* Form Methods */
        public void buildListView(IEnumerable<ITweet> tweets)
        {
            foreach(ITweet tweet in tweets)
            {
                listView1.Items.Add(new ListViewItem(new String[] {  tweet.CreatedAt.ToString(), tweet.FullText, tweet.FavoriteCount.ToString(), tweet.RetweetCount.ToString(), tweet.ReplyCount.ToString(), tweet.Url }));
            }
        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = 0;

            for(int i = 0; i < listView1.Items.Count; i++)
            {
                if(listView1.Items[i].Selected == true)
                {
                    index = i;
                }
            }

            fillInSideBarData(listView1.Items[index]);
        }

        public void fillInSideBarData(ListViewItem item)
        {
            txtCreation.Text = item.SubItems[0].Text;
            txtTweet.Text = item.SubItems[1].Text;
            lblRetweet.Text = item.SubItems[3].Text;
            lblLikes.Text = item.SubItems[2].Text;
            lblReplies.Text = item.SubItems[4].Text;
            currentURL = item.SubItems[5].Text;
        }
    }
}
