﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitter_Test_Program
{
    class SendTweet
    {
        public String tweet;
        public DateTime dateTime;
        public SendTweet(String tweet, DateTime dateTime)
        {
            this.tweet = tweet;
            this.dateTime = dateTime;
        }
    }
}
