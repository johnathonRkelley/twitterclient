﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tweetinvi;
using Tweetinvi.Models;

namespace Twitter_Test_Program
{
    public partial class frmDashboard : Form
    {
        IAuthenticatedUser authenticatedUser;

        public frmDashboard(IAuthenticatedUser authenticatedUser)
        {
            InitializeComponent();
            this.authenticatedUser = authenticatedUser;

            fillInDataFields();
            fillInListViewsWithTimelines();
        }

        /* Button Clicks */
        private void btnHomeTimeline_Click(object sender, EventArgs e)
        {
            int index = 0;

            for (int i = 0; i < listHomeTimeline.Items.Count; i++)
            {
                if (listHomeTimeline.Items[i].Selected == true)
                {
                    index = i;
                }
            }

            openTwitterLinks(listHomeTimeline.Items[index].SubItems[2].Text);
        }
        private void btnRecentTweets_Click(object sender, EventArgs e)
        {
            int index = 0;

            for (int i = 0; i < listPreviousTweets.Items.Count; i++)
            {
                if (listPreviousTweets.Items[i].Selected == true)
                {
                    index = i;
                }
            }

            openTwitterLinks(listPreviousTweets.Items[index].SubItems[2].Text);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            openTwitterLinks("www.twitter.com/" + authenticatedUser.ScreenName);
        }

        /* Form Methods */
        public void openTwitterLinks(String url)
        {
            System.Diagnostics.Process.Start(url);
        }

        public void fillInDataFields()
        {
            lblAccountName.Text = authenticatedUser.Name;
            lblAccount.Text = "@" + authenticatedUser.ScreenName;
            lblFollowers.Text = authenticatedUser.FollowersCount.ToString();
            lblFollowing.Text = authenticatedUser.FriendsCount.ToString();
            pictureBox1.Load(authenticatedUser.ProfileImageUrl400x400);
            txtBio.Text = authenticatedUser.Description;
        }

        public void fillInListViewsWithTimelines()
        {
            IEnumerable<ITweet> homeTimelineTweets = authenticatedUser.GetHomeTimeline();
            foreach (ITweet tweet in homeTimelineTweets)
            {
                listHomeTimeline.Items.Add(new ListViewItem(new String[] { tweet.CreatedBy.ScreenName, tweet.Text, tweet.Url }));
            }

            IEnumerable<ITweet> userTimelineTweets = authenticatedUser.GetUserTimeline(10);
            foreach (ITweet tweet in userTimelineTweets)
            {
                listPreviousTweets.Items.Add(new ListViewItem(new String[] { tweet.CreatedBy.ScreenName, tweet.Text, tweet.Url }));
            }
        }
    }
}
