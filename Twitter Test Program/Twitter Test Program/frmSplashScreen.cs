﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Twitter_Test_Program
{
    public partial class frmSplashScreen : Form
    {
        public int time;
        public frmSplashScreen()
        {
            InitializeComponent();
            time = 0;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (time == 3)
            {
                string appPath = Path.GetDirectoryName(Application.ExecutablePath);
                string path = appPath + "\\config.txt";
                
                try
                {
                    string jsonFromFile = System.IO.File.ReadAllText(path);
                    Config config = JsonConvert.DeserializeObject<Config>(jsonFromFile);
                    new frmHome(config).Show();
                } catch (FileNotFoundException)
                {
                    new frmConfigValues().Show();
                }

                this.Hide();
                timer1.Enabled = false;
            } else
            {
                time++;
            }
        }
    }
}
