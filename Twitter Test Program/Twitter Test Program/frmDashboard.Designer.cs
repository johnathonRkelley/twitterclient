﻿namespace Twitter_Test_Program
{
    partial class frmDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAccountName = new System.Windows.Forms.Label();
            this.lblAccount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.listHomeTimeline = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Tweet = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listPreviousTweets = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label6 = new System.Windows.Forms.Label();
            this.panLeftTable = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.lblFollowing = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblFollowers = new System.Windows.Forms.Label();
            this.panFollower = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBio = new System.Windows.Forms.TextBox();
            this.btnHomeTimeline = new System.Windows.Forms.Button();
            this.btnRecentTweets = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panLeftTable.SuspendLayout();
            this.panFollower.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblAccountName
            // 
            this.lblAccountName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAccountName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblAccountName.Location = new System.Drawing.Point(4, 45);
            this.lblAccountName.Name = "lblAccountName";
            this.lblAccountName.Size = new System.Drawing.Size(217, 43);
            this.lblAccountName.TabIndex = 1;
            this.lblAccountName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAccount
            // 
            this.lblAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblAccount.Location = new System.Drawing.Point(4, 133);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(217, 43);
            this.lblAccount.TabIndex = 2;
            this.lblAccount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 43);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 43);
            this.label2.TabIndex = 4;
            this.label2.Text = "Screename";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(12, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(225, 225);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // linkLabel1
            // 
            this.linkLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.Location = new System.Drawing.Point(4, 177);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(217, 47);
            this.linkLabel1.TabIndex = 10;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Go to Profile";
            this.linkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(474, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "Home Timeline";
            // 
            // listHomeTimeline
            // 
            this.listHomeTimeline.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.listHomeTimeline.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.Tweet,
            this.columnHeader5});
            this.listHomeTimeline.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listHomeTimeline.FullRowSelect = true;
            this.listHomeTimeline.GridLines = true;
            this.listHomeTimeline.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listHomeTimeline.HideSelection = false;
            this.listHomeTimeline.Location = new System.Drawing.Point(478, 32);
            this.listHomeTimeline.MultiSelect = false;
            this.listHomeTimeline.Name = "listHomeTimeline";
            this.listHomeTimeline.Size = new System.Drawing.Size(510, 244);
            this.listHomeTimeline.TabIndex = 12;
            this.listHomeTimeline.UseCompatibleStateImageBehavior = false;
            this.listHomeTimeline.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Author";
            this.columnHeader1.Width = 159;
            // 
            // Tweet
            // 
            this.Tweet.Text = "Tweet";
            this.Tweet.Width = 376;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "URL";
            this.columnHeader5.Width = 350;
            // 
            // listPreviousTweets
            // 
            this.listPreviousTweets.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.listPreviousTweets.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.listPreviousTweets.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listPreviousTweets.FullRowSelect = true;
            this.listPreviousTweets.GridLines = true;
            this.listPreviousTweets.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listPreviousTweets.HideSelection = false;
            this.listPreviousTweets.Location = new System.Drawing.Point(478, 308);
            this.listPreviousTweets.MultiSelect = false;
            this.listPreviousTweets.Name = "listPreviousTweets";
            this.listPreviousTweets.Size = new System.Drawing.Size(510, 244);
            this.listPreviousTweets.TabIndex = 14;
            this.listPreviousTweets.UseCompatibleStateImageBehavior = false;
            this.listPreviousTweets.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Author";
            this.columnHeader2.Width = 159;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Tweet";
            this.columnHeader3.Width = 376;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "URL";
            this.columnHeader4.Width = 350;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(474, 279);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(172, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Recent Sent Tweets";
            // 
            // panLeftTable
            // 
            this.panLeftTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.panLeftTable.ColumnCount = 1;
            this.panLeftTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panLeftTable.Controls.Add(this.label1, 0, 0);
            this.panLeftTable.Controls.Add(this.lblAccountName, 0, 1);
            this.panLeftTable.Controls.Add(this.label2, 0, 2);
            this.panLeftTable.Controls.Add(this.lblAccount, 0, 3);
            this.panLeftTable.Controls.Add(this.linkLabel1, 0, 4);
            this.panLeftTable.Location = new System.Drawing.Point(243, 9);
            this.panLeftTable.Name = "panLeftTable";
            this.panLeftTable.RowCount = 5;
            this.panLeftTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.9992F));
            this.panLeftTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.9992F));
            this.panLeftTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.9992F));
            this.panLeftTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.9992F));
            this.panLeftTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0032F));
            this.panLeftTable.Size = new System.Drawing.Size(225, 225);
            this.panLeftTable.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(219, 35);
            this.label3.TabIndex = 5;
            this.label3.Text = "Account Followers";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFollowing
            // 
            this.lblFollowing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFollowing.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFollowing.Location = new System.Drawing.Point(3, 105);
            this.lblFollowing.Name = "lblFollowing";
            this.lblFollowing.Size = new System.Drawing.Size(219, 38);
            this.lblFollowing.TabIndex = 8;
            this.lblFollowing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(219, 35);
            this.label4.TabIndex = 6;
            this.label4.Text = "Account Following";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFollowers
            // 
            this.lblFollowers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFollowers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFollowers.Location = new System.Drawing.Point(3, 35);
            this.lblFollowers.Name = "lblFollowers";
            this.lblFollowers.Size = new System.Drawing.Size(219, 35);
            this.lblFollowers.TabIndex = 7;
            this.lblFollowers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panFollower
            // 
            this.panFollower.ColumnCount = 1;
            this.panFollower.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panFollower.Controls.Add(this.lblFollowers, 0, 1);
            this.panFollower.Controls.Add(this.label4, 0, 2);
            this.panFollower.Controls.Add(this.lblFollowing, 0, 3);
            this.panFollower.Controls.Add(this.label3, 0, 0);
            this.panFollower.Location = new System.Drawing.Point(243, 240);
            this.panFollower.Name = "panFollower";
            this.panFollower.RowCount = 4;
            this.panFollower.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.panFollower.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.panFollower.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.panFollower.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.panFollower.Size = new System.Drawing.Size(225, 143);
            this.panFollower.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label7.Location = new System.Drawing.Point(12, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(225, 37);
            this.label7.TabIndex = 17;
            this.label7.Text = "User Bio";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBio
            // 
            this.txtBio.Enabled = false;
            this.txtBio.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtBio.Location = new System.Drawing.Point(12, 280);
            this.txtBio.Multiline = true;
            this.txtBio.Name = "txtBio";
            this.txtBio.Size = new System.Drawing.Size(225, 103);
            this.txtBio.TabIndex = 18;
            // 
            // btnHomeTimeline
            // 
            this.btnHomeTimeline.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnHomeTimeline.FlatAppearance.BorderSize = 0;
            this.btnHomeTimeline.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHomeTimeline.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeTimeline.Location = new System.Drawing.Point(913, 6);
            this.btnHomeTimeline.Name = "btnHomeTimeline";
            this.btnHomeTimeline.Size = new System.Drawing.Size(75, 23);
            this.btnHomeTimeline.TabIndex = 19;
            this.btnHomeTimeline.Text = "Go To URL";
            this.btnHomeTimeline.UseVisualStyleBackColor = true;
            this.btnHomeTimeline.Click += new System.EventHandler(this.btnHomeTimeline_Click);
            // 
            // btnRecentTweets
            // 
            this.btnRecentTweets.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnRecentTweets.FlatAppearance.BorderSize = 0;
            this.btnRecentTweets.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecentTweets.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecentTweets.Location = new System.Drawing.Point(913, 279);
            this.btnRecentTweets.Name = "btnRecentTweets";
            this.btnRecentTweets.Size = new System.Drawing.Size(75, 23);
            this.btnRecentTweets.TabIndex = 20;
            this.btnRecentTweets.Text = "Go To URL";
            this.btnRecentTweets.UseVisualStyleBackColor = true;
            this.btnRecentTweets.Click += new System.EventHandler(this.btnRecentTweets_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(106)))), ((int)(((byte)(218)))));
            this.panel1.Location = new System.Drawing.Point(12, 389);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(452, 163);
            this.panel1.TabIndex = 21;
            // 
            // frmDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            this.ClientSize = new System.Drawing.Size(1000, 560);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnRecentTweets);
            this.Controls.Add(this.btnHomeTimeline);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtBio);
            this.Controls.Add(this.panFollower);
            this.Controls.Add(this.panLeftTable);
            this.Controls.Add(this.listPreviousTweets);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.listHomeTimeline);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmDashboard";
            this.Text = "frmDashboard";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panLeftTable.ResumeLayout(false);
            this.panFollower.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblAccountName;
        private System.Windows.Forms.Label lblAccount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListView listHomeTimeline;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader Tweet;
        private System.Windows.Forms.ListView listPreviousTweets;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel panLeftTable;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblFollowing;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblFollowers;
        private System.Windows.Forms.TableLayoutPanel panFollower;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBio;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button btnHomeTimeline;
        private System.Windows.Forms.Button btnRecentTweets;
        private System.Windows.Forms.Panel panel1;
    }
}