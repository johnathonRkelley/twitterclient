﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tweetinvi.Models;

namespace Twitter_Test_Program
{
    public partial class frmFriends : Form
    {
        public IEnumerable<IUser> friends;
        public IEnumerable<IUser> followers;
        public List<IUser> friendsOfFollowers;
        IAuthenticatedUser authenticatedUser;
        public frmFriends(IAuthenticatedUser authenticatedUser)
        {
            InitializeComponent();
            this.authenticatedUser = authenticatedUser;

            friends = authenticatedUser.GetFriends(50);
            followers = authenticatedUser.GetFollowers(50);
            friendsOfFollowers = new List<IUser>();

            populateListViewTables();
        }

        /* Button Clicks */
        private void btnTest_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Text.Equals("Friends")) grabDataFromListView(0);
            if (tabControl1.SelectedTab.Text.Equals("Followers")) grabDataFromListView(1);
            if (tabControl1.SelectedTab.Text.Equals("Friends of Followers")) grabDataFromListView(2);
        }

        /* Form Methods */
        private void listview_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Text.Equals("Friends")) fillInSideBarData(listFriends);
            if (tabControl1.SelectedTab.Text.Equals("Followers")) fillInSideBarData(listFollowers);
            if (tabControl1.SelectedTab.Text.Equals("Friends of Followers")) fillInSideBarData(listFriendsOfFollowers);
        }

        /* Form Method Helper Functions */
        public void populateListViewTables()
        {
            foreach (IUser friend in friends)
            {
                listFriends.Items.Add(new ListViewItem(new String[] { friend.Name, getTwitterURL(friend.ScreenName) }));
                foreach (IUser follower in followers)
                {
                    listFollowers.Items.Add(new ListViewItem(new String[] { follower.Name, getTwitterURL(follower.ScreenName) }));
                    if (friend.Name.Equals(follower.Name))
                    {
                        listFriendsOfFollowers.Items.Add(new ListViewItem(new String[] { friend.Name, getTwitterURL(friend.ScreenName) }));
                        friendsOfFollowers.Add(follower);
                    }
                }
            }

        }
        public void grabDataFromListView(int listViewID)
        {
            ListView currentListView = null;

            switch (listViewID)
            {
                case 0:
                    currentListView = listFriends;
                    break;
                case 1:
                    currentListView = listFollowers;
                    break;
                case 2:
                    currentListView = listFriendsOfFollowers;
                    break;
            }

            int index = getSelectedIndexFromListView(currentListView);

            openTwitterLinkProfile(currentListView.Items[index].SubItems[1].Text);
        }
        public string getTwitterURL(string screenName)
        {
            return "www.twitter.com/" + screenName;
        }
        public void openTwitterLinkProfile(string url)
        {
            System.Diagnostics.Process.Start(url);
        }
        public int getSelectedIndexFromListView(ListView currentListView)
        {
            int index = 0;

            for (int i = 0; i < currentListView.Items.Count; i++)
            {
                if (currentListView.Items[i].Selected)
                {
                    index = i;
                }
            }

            return index;
        }
        public void fillInSideBarData(ListView currentListView)
        {
            int index = getSelectedIndexFromListView(currentListView);
            lblProfileName.Text = currentListView.Items[index].SubItems[0].Text;
            IUser user = findIUserForProfile(lblProfileName.Text);
            pictureBox1.Load(user.ProfileImageUrl400x400);
            txtUserBio.Text = user.Description;
            lblScreenName.Text = "@" + user.ScreenName;
        }
        public IUser findIUserForProfile(String profileName)
        {
            IUser user = null;

            foreach (IUser friend in friends)
            {
                if (friend.Name.Equals(profileName)) user = friend;
            }

            foreach (IUser follower in followers)
            {
                if (follower.Name.Equals(profileName)) user = follower;
            }

            return user;
        }
    }
}
