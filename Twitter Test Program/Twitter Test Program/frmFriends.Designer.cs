﻿namespace Twitter_Test_Program
{
    partial class frmFriends
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtUserBio = new System.Windows.Forms.TextBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panUserInfo = new System.Windows.Forms.TableLayoutPanel();
            this.lblProfileName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblScreenName = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panBody = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listFriends = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.listFollowers = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.listFriendsOfFollowers = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1.SuspendLayout();
            this.panUserInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panBody.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtUserBio);
            this.panel1.Controls.Add(this.btnTest);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.panUserInfo);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(255, 560);
            this.panel1.TabIndex = 10;
            // 
            // txtUserBio
            // 
            this.txtUserBio.Location = new System.Drawing.Point(33, 387);
            this.txtUserBio.Multiline = true;
            this.txtUserBio.Name = "txtUserBio";
            this.txtUserBio.Size = new System.Drawing.Size(200, 80);
            this.txtUserBio.TabIndex = 12;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(89, 495);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(83, 23);
            this.btnTest.TabIndex = 10;
            this.btnTest.Text = "Go To Profile";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(255, 63);
            this.label1.TabIndex = 14;
            this.label1.Text = "Follower Information";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panUserInfo
            // 
            this.panUserInfo.ColumnCount = 1;
            this.panUserInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panUserInfo.Controls.Add(this.lblProfileName, 0, 0);
            this.panUserInfo.Controls.Add(this.label2, 0, 2);
            this.panUserInfo.Controls.Add(this.lblScreenName, 0, 1);
            this.panUserInfo.Location = new System.Drawing.Point(33, 281);
            this.panUserInfo.Name = "panUserInfo";
            this.panUserInfo.RowCount = 3;
            this.panUserInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.panUserInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.panUserInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.panUserInfo.Size = new System.Drawing.Size(200, 100);
            this.panUserInfo.TabIndex = 13;
            // 
            // lblProfileName
            // 
            this.lblProfileName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblProfileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProfileName.Location = new System.Drawing.Point(3, 0);
            this.lblProfileName.Name = "lblProfileName";
            this.lblProfileName.Size = new System.Drawing.Size(194, 33);
            this.lblProfileName.TabIndex = 4;
            this.lblProfileName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(194, 34);
            this.label2.TabIndex = 6;
            this.label2.Text = "User Bio";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lblScreenName
            // 
            this.lblScreenName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblScreenName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScreenName.Location = new System.Drawing.Point(3, 33);
            this.lblScreenName.Name = "lblScreenName";
            this.lblScreenName.Size = new System.Drawing.Size(194, 33);
            this.lblScreenName.TabIndex = 7;
            this.lblScreenName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(33, 75);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 200);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // panBody
            // 
            this.panBody.Controls.Add(this.tabControl1);
            this.panBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panBody.Location = new System.Drawing.Point(255, 0);
            this.panBody.Name = "panBody";
            this.panBody.Size = new System.Drawing.Size(745, 560);
            this.panBody.TabIndex = 11;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(745, 560);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.listFriends);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(737, 534);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Friends";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // listFriends
            // 
            this.listFriends.BackColor = System.Drawing.Color.White;
            this.listFriends.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listFriends.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listFriends.FullRowSelect = true;
            this.listFriends.GridLines = true;
            this.listFriends.HideSelection = false;
            this.listFriends.Location = new System.Drawing.Point(3, 3);
            this.listFriends.Name = "listFriends";
            this.listFriends.Size = new System.Drawing.Size(731, 528);
            this.listFriends.TabIndex = 0;
            this.listFriends.UseCompatibleStateImageBehavior = false;
            this.listFriends.View = System.Windows.Forms.View.Details;
            this.listFriends.SelectedIndexChanged += new System.EventHandler(this.listview_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Twitter Name";
            this.columnHeader1.Width = 200;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "URL";
            this.columnHeader2.Width = 400;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.listFollowers);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(737, 534);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Followers";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // listFollowers
            // 
            this.listFollowers.BackColor = System.Drawing.Color.White;
            this.listFollowers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.listFollowers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listFollowers.FullRowSelect = true;
            this.listFollowers.GridLines = true;
            this.listFollowers.HideSelection = false;
            this.listFollowers.Location = new System.Drawing.Point(3, 3);
            this.listFollowers.Name = "listFollowers";
            this.listFollowers.Size = new System.Drawing.Size(731, 528);
            this.listFollowers.TabIndex = 0;
            this.listFollowers.UseCompatibleStateImageBehavior = false;
            this.listFollowers.View = System.Windows.Forms.View.Details;
            this.listFollowers.SelectedIndexChanged += new System.EventHandler(this.listview_SelectedIndexChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Twitter Name";
            this.columnHeader3.Width = 200;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "URL";
            this.columnHeader4.Width = 400;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.listFriendsOfFollowers);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(737, 534);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Friends of Followers";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // listFriendsOfFollowers
            // 
            this.listFriendsOfFollowers.BackColor = System.Drawing.Color.White;
            this.listFriendsOfFollowers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6});
            this.listFriendsOfFollowers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listFriendsOfFollowers.FullRowSelect = true;
            this.listFriendsOfFollowers.GridLines = true;
            this.listFriendsOfFollowers.HideSelection = false;
            this.listFriendsOfFollowers.Location = new System.Drawing.Point(0, 0);
            this.listFriendsOfFollowers.Name = "listFriendsOfFollowers";
            this.listFriendsOfFollowers.Size = new System.Drawing.Size(737, 534);
            this.listFriendsOfFollowers.TabIndex = 0;
            this.listFriendsOfFollowers.UseCompatibleStateImageBehavior = false;
            this.listFriendsOfFollowers.View = System.Windows.Forms.View.Details;
            this.listFriendsOfFollowers.SelectedIndexChanged += new System.EventHandler(this.listview_SelectedIndexChanged);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Twitter Name ";
            this.columnHeader5.Width = 200;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "URL";
            this.columnHeader6.Width = 400;
            // 
            // frmFriends
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 560);
            this.Controls.Add(this.panBody);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmFriends";
            this.Text = "frmFriends";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panUserInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panBody.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtUserBio;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel panUserInfo;
        private System.Windows.Forms.Label lblProfileName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblScreenName;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panBody;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ListView listFriends;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView listFollowers;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListView listFriendsOfFollowers;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
    }
}