﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tweetinvi;
using Tweetinvi.Models;

namespace Twitter_Test_Program
{
    public partial class frmConfigValues : Form
    {
        public frmConfigValues()
        {
            InitializeComponent();
        }

        /* Form Movement Controls */
        private bool mouseDown;
        private Point lastLocation;
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        /* Button Click */
        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            TwitterCredentials creds = new TwitterCredentials(txtConsumerAPIKey.Text, txtConsumerSecretKey.Text, txtAccessToken.Text, txtAccessTokenSecret.Text);
            Auth.SetCredentials(creds);

            try
            {
                IAuthenticatedUser authenticatedUser = User.GetAuthenticatedUser(creds);
                String name = authenticatedUser.Name;

                lblStatus.Text = "Your account name on Twitter is :\n" + name + "\nNow you can save your data";
                btnSaveData.Enabled = true;
            }
            catch (NullReferenceException)
            {
                lblStatus.Text = "Something went wrong, make sure you put in the right data.";
            }
        }
        private void btnSaveData_Click(object sender, EventArgs e)
        {
            Config config = createConfigObject();

            saveConfigObjectToJSONFile(config);

            new frmHome(config).Show();
            this.Hide();
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /* Form Methods */
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            String url = "https://developer.twitter.com/en";
            System.Diagnostics.Process.Start(url);
        }
        /* Form Helper Methods */
        public void saveConfigObjectToJSONFile(Config config)
        {
            string json = JsonConvert.SerializeObject(config);

            string appPath = Path.GetDirectoryName(Application.ExecutablePath);
            string path = appPath + "\\config.txt";

            System.IO.File.WriteAllText(path, json);
        }

        public Config createConfigObject()
        {
            Config config = new Config();

            config.API_KEY = txtConsumerAPIKey.Text;
            config.API_SECRET_KEY = txtConsumerSecretKey.Text;
            config.ACCESS_TOKEN = txtAccessToken.Text;
            config.ACCESS_TOKEN_SECRET = txtAccessTokenSecret.Text;

            return config;
        }
    }
}
